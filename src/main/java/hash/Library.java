package hash;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Library {

    private long id;
    private long nrBooks;
    private long signUpDays;
    private long shipBooksDay;
    private Long[] books;
    private long booksToScan;
    private List<Long> scannedBooks = new ArrayList();


    public Library(long id, long nrBooks, long signUpDays, long shipBooksDay, Long[] books) {
        this.id = id;
        this.nrBooks = nrBooks;
        this.signUpDays = signUpDays;
        this.shipBooksDay = shipBooksDay;
        this.books = books;
    }


    public void setBooksToScan(long booksToScan) {
        this.booksToScan = booksToScan;
    }

    public long getBooksToScan() {
        return booksToScan;
    }
}
