package hash;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Main {

    static long numberOfBooks, numberOfLibraries, daysOfScanning;
    static long[] score;

    public static void main(String[] args) throws Exception {
        ClassLoader classLoader = new Main().getClass().getClassLoader();
        File fileIn = new File(classLoader.getResource("e_so_many_books.txt").getFile());
        //readInput(fileIn);

        List<Library> results = compute(readInput(fileIn));

        File fileOut = new File("output_e.txt");
        write(results, fileOut);

    }

    private static List<Library> compute(List<Library> libraries) {

        Collections.sort(libraries, Comparator.comparingLong(Library::getSignUpDays));
        List<Library> libsToScan = new ArrayList<>();
        long sumForScanning = 0;
        boolean skip = false;
        for (Library lib : libraries) {
            if (skip) {
                skip = false;
                continue;
            }
            skip = true;
            if (sumForScanning + lib.getSignUpDays() < daysOfScanning) {
                sumForScanning += lib.getSignUpDays();
                libsToScan.add(lib);
            } else {
                break;
            }
        }


        long remainingDaysOfScanning = daysOfScanning;
        for (Library lib : libsToScan) {
            try {
                remainingDaysOfScanning = remainingDaysOfScanning - lib.getSignUpDays();
                long l = remainingDaysOfScanning * lib.getShipBooksDay();

                lib.setBooksToScan(l > lib.getBooks().length ? lib.getBooks().length : l);

            } catch (Exception e) {
                System.out.println();
            }
        }

        for (Library lib : libsToScan) {
            Arrays.sort(lib.getBooks(), Collections.reverseOrder());
        }


        long[] markedBooks = new long[(int) numberOfBooks];

        for (int i = 0; i < daysOfScanning; i++) {
            for (Library lib : libsToScan) {
                if (i < lib.getSignUpDays()) {
                    continue;
                }

                long startIndex = ((i - lib.getSignUpDays()) * lib.getShipBooksDay());
                long written=0;
                for (long j = startIndex; j < lib.getBooks().length; j++) {
                    if(written == lib.getShipBooksDay()) {
                        break;
                    }
                    long book = lib.getBooks()[(int) j];
                    if (0 == markedBooks[(int) book]) {
                        written++;
                        markedBooks[(int) book] = 1;
                        lib.getScannedBooks().add(book);
                    }
                }
            }
        }


        return libsToScan;
    }

    private static List<Library> readInput(File inputFile) throws IOException {
        BufferedReader reader;

        reader = new BufferedReader(new FileReader(inputFile));

        String[] split = reader.readLine().split(" ");
        numberOfBooks = Long.valueOf(split[0]);
        numberOfLibraries = Long.valueOf(split[1]);
        daysOfScanning = Long.valueOf(split[2]);

        long[] books = new long[(int) numberOfBooks];
        split = reader.readLine().split(" ");
        for (int i = 0; i < numberOfBooks; i++) {
            books[i] = Long.valueOf(split[i]);
        }

        score = books;

        List<Library> libraries = new ArrayList<>();

        for (int libraryIndex = 0; libraryIndex < numberOfLibraries; libraryIndex++) {
            split = reader.readLine().split(" ");
            String[] booksStr = reader.readLine().split(" ");
            Long[] b = new Long[Integer.valueOf(split[0])];
            for (int bookIndex = 0; bookIndex < Long.valueOf(split[0]); bookIndex++) {
                b[bookIndex] = Long.valueOf(booksStr[bookIndex]);
            }
            libraries.add(new Library(libraryIndex, Long.valueOf(split[0]), Long.valueOf(split[1]), Long.valueOf(split[2]), b));
        }

        reader.close();
        return libraries;
    }

    private static void write(List<Library> libraries, File outFile) throws Exception {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), StandardCharsets.UTF_8));

        int size = 0;
        for (Library library : libraries) {
            if (library.getScannedBooks().size() == 0) {
                continue;
            }
            size++;
        }

        writer.write(size + "");
        writer.newLine();
        for (Library library : libraries) {
            if (library.getScannedBooks().size() == 0) {
                continue;
            }
            writer.write(library.getId() + " " + (library.getScannedBooks().size()));
            writer.newLine();


            for (Long book : library.getScannedBooks()) {
                writer.write("" + book);
                writer.write(" ");
            }

            writer.newLine();
        }
        writer.close();
    }


}
