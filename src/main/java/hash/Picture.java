package hash;



import java.util.List;

public class Picture {

    private int initialPos;
    private int tagNumber;
    private List<String> tagSet;
    private boolean horizontal;

    public Picture(boolean horizontal, int tagNumber, List<String> tagSet, int initialPos) {
        this.tagSet = tagSet;
        this.tagNumber = tagNumber;
        this.horizontal = horizontal;
        this.initialPos = initialPos;
    }

    public int computeInterest(Picture picture) {
        int aMinusB = 0;
        int bMinusA = 0;
        int aIntB = 0;

        int poz = 0;
        int i = 0;
        int j = 0;
        while (i < this.tagSet.size() && j < picture.tagSet.size()){
            if(this.tagSet.get(i).equals(picture.tagSet.get(j))){
                i++;
                j++;
                aIntB++;
            }else if(this.tagSet.get(i).compareTo(picture.tagSet.get(j)) > 0){
                j++;
                bMinusA++;
            }else{
                i++;
                aMinusB++;
            }
        }

        if(i < this.tagSet.size()){
            aMinusB += this.tagSet.size() - i;
        }

        if(j < picture.tagSet.size()){
            bMinusA += picture.tagSet.size() - j;
        }

        int interest = Math.min(Math.min(aIntB, aMinusB), bMinusA);

        return interest;
    }
}
